class Day1
  def part1(string_input)
    input = string_input.split("").map { |item| item.to_i }
    i = 0
    sum = 0
    while input[i] != nil
      sum += input[i] unless input[i+1] != input[i]
      if input[i+1] == nil
        if input.first == input[i] 
          sum += input[i]
        end
      end
      i += 1
    end
    return sum 
  end

  def part2(string_input)
    input = string_input.split("").map { |item| item.to_i }
    i = 0
    sum = 0
    input_size = input.length
    while input[i] != nil
      sum += input[i] unless input[(i + input_size/2)%input_size] != input[i]
      i += 1
    end
    return sum
  end
end
