class KnotHash
  attr_accessor :list, :lengths

  def initialize(lengths, max_int = 255)
    @lengths = lengths.strip.chars.map(&:to_i)
    @list = (0..max_int).to_a
    #@lengths += [17, 31, 73, 47, 23]
  end

  def generate_hash
    added_lengths = [17, 31, 73, 47, 23] # the ones they want added for some reason
    @lengths = @lengths.map(&:to_s).join(",")
    @lengths = @lengths.strip.split("").map(&:ord).each.to_a + added_lengths

    run_rounds(64)
    convert_to_hex
  end


  def run_rounds(n = 1)
    position, skip_size = 0, 0
    n.times do |i|
      #puts "position: #{position}, skip size: #{skip_size}"
      position, skip_size = run_round(position, skip_size)
    end
  end

  def convert_to_hex
    hex_string = ''
    xor_list = gen_xor_list
    xor_list.each do |item|
      #p "%02x" % item
      hex_string << "%02x" % item
    end
    hex_string
  end

  private
  def run_round(starting_position, starting_skip_size)
    skip_size = starting_skip_size
    position = starting_position
    indices = []
    @lengths.each do |length|
      #puts "---------------before applying length #{length}: #{@list}"
      #puts "length: #{length}, position: #{position}, skip_size: #{skip_size}"
      circle_array = []
      @list.cycle(2) { |val| circle_array << val }
      sub_list = circle_array.slice(position, length)
      #puts "sub_list: #{sub_list}"
      indices = []
      sub_list.each { |value| indices << @list.index(value) }
      #puts "indices to change: #{indices}"
      sub_list.reverse!
      sub_list.length.times do |i|
        @list[indices[i]] = sub_list[i]
      end
      #puts "moving forward by #{skip_size + length}"
      position = (position + skip_size + length) % @list.length
      skip_size += 1
      #puts "---------------after applying length #{length}: #{@list}"
    end
    return position, skip_size
  end

  def xor_sequence(numbers)
    xor_element = numbers[0] ^ numbers[1]
    numbers.drop(2).each do |number|
      xor_element = xor_element ^ number
    end
    xor_element
  end

  def gen_xor_list
    # group into groups of 16
    groups = @list.each_slice(16) 
    # xor the numbers in each group
    xor_list = []
    groups.each do |group|
      xor_list << xor_sequence(group)
    end
    xor_list
  end
end



class Day10

  def part1(string_input)
    p input
    knot_hash = KnotHash.new(input)
    p knot_hash.lengths
    knot_hash.run_rounds(1)
    knot_hash.list[0] * knot_hash.list[1]
  end

  def part2(string_input)
    # conver to ascii and add to @length_list
    # string_input.each_byte { |char| @length_list << char }
    # @length_list = @length_list.first(@length_list.size - 1) #remove the last element

    knot_hash = KnotHash.new(string_input)
    knot_hash.generate_hash

    # run 64 rounds
    #position, skip_size = 0, 0
    #64.times do |i|
    #  puts "position: #{position}, skip size: #{skip_size}"
    #  position, skip_size = run_round(position, skip_size)
    #end

  end


  private

  def run_n_times(n)
    postion, skip_size = 0, 0
    n.times { |i| position, skip_size = run_round(position, skip_size) }
    @list
  end



end

