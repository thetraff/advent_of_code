require 'set'
class Network
  attr_reader :size, :group

  def initialize(string_input)
    @network_list = {}
    @group = []
    string_input.split("\n").each do |pipe|
      pipe_values = pipe.split("<->")
      host_pipe = pipe_values[0].to_i
      connection_list = pipe_values[1]
      if connection_list.length == 1
        @network_list[host_pipe] = connection_list.to_i
      else
        @network_list[host_pipe] = connection_list.split(",").map(&:to_i)
      end
    end
    @size = @network_list.length
  end

  def pipe(pipe_id)
    @network_list[pipe_id]
  end
  
  def clear_group
    @group = []
  end

  def find_group(pipe_id)
    @group |= [pipe_id]
    self.pipe(pipe_id).each do |pipe_connection|
      if !@group.include?(pipe_connection)
        @group << pipe_connection
        self.find_group(pipe_connection)
      end
    end
    @group
  end

end

class Day12
  
  def part1(input)
    pipe_network = Network.new(input)
    pipe_network.find_group(0).length
  end

  def part2(input)
    groups = []
    pipe_network = Network.new(input)
    pipe_network.size.times do |pipe_id|
      groups << pipe_network.find_group(pipe_id).to_set
      pipe_network.clear_group
    end
    groups.uniq.length
  end
end
