def search_sequence(multiple)
  Enumerator.new do |y|
    a = 1
    loop do
      y << a
      a = (a + 1) % multiple == 0 ? a + 2 : a + 1
    end
  end
end

class Layer
  attr_reader :scanner, :current, :range, :multiple
  
  def initialize(range)
    @range = range
    @positions = (0 .. @range-1).to_a + (1 .. @range-2).to_a.reverse
    @position_tracker = 0
    @current = @positions[@position_tracker]
    @scanner = Array.new(@range, 0)
    @scanner[0] = 1 if @range > 0
    @multiple = 2 * @range - 2
    @catch_positions = 0
  end

  def increment
    if @range != 0
      @scanner[@current] = 0
      @position_tracker = (@position_tracker + 1) % @positions.length
      @current = @positions[@position_tracker]
      @scanner[@current] = 1
    end
  end

  def position_at(time_step)
    time_step % @positions.length
  end
  
  def find_catch_position(distance)
    @catch_position = -(distance % @multiple)
    @catch_position = @positions.length + @catch_position if @catch_position != 0
  end

  def caught?(delay_time)
    caught = false
    return caught if @range == 0
    if self.position_at(delay_time) == @catch_position
      caught = true
    else
      caught = false
    end
    caught
    #end
  end


  def reset
    @scanner = @scanner.fill(0)
    @scanner[0] = 1 if @range > 0
    @position_tracker = 0
    @current = @positions[@position_tracker]
  end
end

class Firewall
  attr_reader :layers, :depth

  def initialize(layer_data)
    @depth = 0
    @layers = []
    @depth = layer_data[-1][0].to_i
    layer_hash = (0..@depth).each_with_object({}) { |index, hash| hash[index] = 0 }
    layer_data.each do |pair|
      layer_hash[pair[0].to_i] = pair[1].to_i
    end

    layer_hash.each do |layer, range|
      self.add_layer(range)
    end
  end

  def add_layer(range)
    layer = Layer.new(range)
    layers.push(layer)
  end

  def display
    layers.each do |layer|
      p layer.scanner
    end
  end

  def increment
    layers.each { |layer| layer.increment }
  end

  def reset
    layers.each { |layer| layer.reset }
  end
end


class Day13

  def traverse_firewall(firewall)
    severity_sum = 0
    firewall.depth.times do |curr_depth|
      layer = firewall.layers[curr_depth]
      if layer.current == 0
        severity_sum += curr_depth * layer.range
      end
      firewall.increment
    end
    severity_sum
  end

  def part1(input)
    layer_data = input.scan(/(\d+): (\d+)/)
    firewall = Firewall.new(layer_data)
    traverse_firewall(firewall)
  end

  def part2(input)
    layer_data = input.scan(/(\d+): (\d+)/)
    firewall = Firewall.new(layer_data)
    
    #firewall.display
    is_caught = false 
    firewall.layers.each do |layer|
      layer.find_catch_position(firewall.layers.index(layer))
    end

    first_multiple = firewall.layers[0].multiple
    search_sequence(first_multiple).each do |delay_time|
      #p delay_time
      firewall.layers.each do |layer|
        if layer.caught?(delay_time)
          is_caught = true
          break
        end
      end
      if !is_caught
        return delay_time
      end
      is_caught = false

    end

  end

end
