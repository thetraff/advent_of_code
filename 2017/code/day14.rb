require_relative 'day10.rb'


class Grid 
  attr_accessor :grid, :binary_grid
  
  def initialize(string, size = 128)
    @grid_strings = []
    size.times do |row_num|
      @grid_strings << "#{string}-#{row_num}"
    end
  end

  def gen_binary_grid
    @binary_grid = []
    @hash_grid.each do |hex_string|
      @binary_grid << convert_hex_to_binary(hex_string)
    end
  end

  def gen_knot_hashes
    @hash_grid = []
    @grid_strings.each do |string|
      string_hash = KnotHash.new(string)
      @hash_grid << string_hash.generate_hash
    end
  end

  def count_used_squares
    used_count = 0
    @binary_grid.each do |row|
      row.each do |square|
        used_count += 1 if square == 1
      end
    end
    used_count
  end


  private
  def convert_hex_to_binary(hex_string)
    binary_string = ""
    hex_string.split("").each do |char|
      binary_string << char.hex.to_s(2).rjust(char.size*4, '0')
    end
    binary_string.chars.map(&:to_i)
  end

    

end

class Day14

  def part1(string_input)
    disk = Grid.new(string_input.strip)
    disk.gen_knot_hashes
    disk.gen_binary_grid
    disk.count_used_squares

  end

  def part2(string_input)
  end
end
