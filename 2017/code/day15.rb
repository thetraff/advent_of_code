class Generator
  attr_accessor :current
  

  def initialize(init_value, factor)
    @init_value = init_value
    @previous_value = init_value
    @factor = factor
  end

  def gen_next
    @current = (@previous_value * @factor) % 2147483647
    @previous_value = @current
  end

  def last_16_bits
    @current.to_s(2).split(//).last(16)
    #@current.to_s(2).rjust(32, '0').to_i(2) << 16
  end

  private
  def reset
    @current = nil
    @previous_value = @init_value
  end

end


class Day15

  def part1(string_input)
    initial_values = string_input.scan(/\d+/).map(&:to_i)
    gen_a = Generator.new(initial_values[0], 16807)
    gen_b = Generator.new(initial_values[1], 48271)

    matching_pairs = 0

    40000000.times do |i|
      gen_a.gen_next
      gen_b.gen_next
      #p gen_a.last_16_bits, gen_a.current
      #p gen_b.last_16_bits, gen_b.current
      #puts "----"
      if gen_a.last_16_bits == gen_b.last_16_bits
        matching_pairs += 1
      end
    end
    matching_pairs
  end

  def part2(string_input)

  end
end


