class DanceLine
  attr_accessor :programs
  
  def initialize(line_size)
    @programs = []
    ("a".."z").each_with_index do |program, index|
      @programs << program
      break if index >= line_size - 1
    end
  end

  def perform(dance_string)
    #p dance_string
    if dance_string[0] == "s"
      spin(dance_string.scan(/\d+/)[0].to_i)
    end
    if dance_string[0] == "x"
      positions = dance_string.scan(/\d+/).map(&:to_i)
      exchange(positions[0], positions[1])
    end
    if dance_string[0] == "p"
      program_letters = dance_string.slice(1..-1).scan(/[a-z]/)
      partner(program_letters[0], program_letters[1])
    end
  end
  
  def spin(number)
    #puts " I'll try spinning, that's a good trick!"
    move_to_front = @programs.pop(number)
    @programs = move_to_front + @programs
  end

  def exchange(position_a, position_b)
    #puts "I'd like to make an exchange"
    @programs[position_a], @programs[position_b] = @programs[position_b], @programs[position_a]
  end

  def partner(program_a, program_b)
    #puts "I'm so lonely i need a partner"
    p_a = @programs.index(program_a)
    p_b = @programs.index(program_b)
    @programs[p_a], @programs[p_b] = program_b, program_a
  end

  def to_s
    @programs.join("")
  end
end

class Day16

  def part1(input)
    dance_line = DanceLine.new(16)
    dance_moves = input.split(',')
    dance_moves.each do |move|
      dance_line.perform(move)
      #p dance_line.programs
    end
    dance_line
  end

  def part2(input)
    dance_line = DanceLine.new(16)
    permutations = []
    repeat_index = 0
    dance_moves = input.split(',')
    1000000000.times do |i|
      dance_moves.each do |move|
        dance_line.perform(move)
      end
      if permutations.include?(dance_line.to_s)
        p permutations
        repeat_index = i + 1
        p "we have a match at index: #{repeat_index}, with #{dance_line.to_s}, the first occurence was at: #{permutations.index(dance_line.to_s)}"
        break
      end
      permutations << dance_line.to_s
    end
    times_to_dance = 1000000000 % repeat_index
    times_to_dance.times do |i|
      dance_moves.each do |move|
        dance_line.perform(move)
      end
    end
    dance_line
  end
end
