class SpinLock
  attr_accessor :list, :current_position

  def initialize(step_size)
    @step_size = step_size
    @list = [0]
    @count = 1
    @current_position = 0
  end

  def cycle_n_times(n)
    n.times do |i|
      puts "at step #{i}" if i % 100000 == 0
      cycle
    end
  end

  def cycle
    @current_position = (@current_position + @step_size) % @list.length
    insert(@count)
  end

  private
  def insert(value)
    @list = @list.insert(@current_position + 1, @count)
    @current_position += 1
    @count += 1
  end
end

def break_sequence(break_index, step_size)
  Enumerator.new do |y|
    current = 0
    length = 1
    loop do
      current = (current + step_size) % length
      y << length if current == break_index
      current += 1
      length += 1
    end
  end
end

class Day17

  def part1(input)
    spin_lock = SpinLock.new(input.to_i)
    spin_lock.cycle_n_times(2017)
    spin_lock.list[spin_lock.current_position + 1]
  end

  def part2(input)
    break_num = 0
    break_sequence(0, input.to_i).each do |break_number|
      p break_number
      break if break_number >= 50000000
      break_num = break_number
    end
    break_num
  end
end
