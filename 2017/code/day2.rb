class Day2

  def part1(input)
    input.split("\n").map { |row| min_max_row(row) }.inject(0) { |sum, item| sum + item }
    

  end

  def part2(input)
    input.split("\n").map { |row| divisible_row(row) }.inject(0) { |sum, item| sum + item }
  end

  private
  def min_max_row(row)
    row = row.split.map(&:to_i)
    row.max - row.min
  end

  def divisible_row(row)
    row = row.split.map(&:to_i)
    row.each do |num|
      row.each do |div|
        return div/num if div % num == 0 && div != num
      end
    end
  end


end
