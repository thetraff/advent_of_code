class Day3
  def part1(input)
    mem_spot = input.to_i
    return 0 if mem_spot == 1
    root = Math.sqrt(mem_spot).ceil

    limit = root.odd? ? root : root + 1
    layer = (((limit - 2) + 1) / 2) 

    lower_bound = ((limit - 2) ** 2) + 1
    val_options = (layer .. (2 * layer)).to_a
    val_options = val_options.reverse.rotate.first(val_options.length - 2) + val_options

    (1..((mem_spot - lower_bound) / val_options.length)).each do
      val_options += val_options
    end

    val_options[mem_spot - lower_bound]

  end

  def part2(input)
    x,y = 0,0
    dx, dy = 0,-1
    grid_size = 999
    grid = { }
    grid[[0,0]] = 1
    adjacent_cells = [[0,1],[1,1],[1,0],[1,-1],[0,-1],[-1,-1],[-1,0],[-1,1]]

    (0 .. (grid_size ** 2)).each do |num|
      if ((-grid_size / 2 < x) && (x <= grid_size / 2)) && ((-grid_size / 2 < y) && (y <= grid_size/2))
        grid[[x,y]] ||= 0 if x != 0 || y != 0 

        adjacent_cells.each do |cell|
          cell_item = grid[[x + cell[0], y + cell[1]]]
          grid[[x,y]] += cell_item if cell_item != nil
        end
        return grid[[x,y]] if grid[[x,y]] > input.to_i

      end

      if (x == y) || (x < 0 && x == -y) || (x > 0 && x == 1-y)
        dx, dy = -dy, dx
      end
      x, y = x + dx, y + dy
    end

    grid

  end
end



