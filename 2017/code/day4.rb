class Day4
  
  def initialize
    @prime_dict = {}
    @prime_dict['a'] = 2
    prime = true
    ('a' .. 'z').to_a.each_cons(2) do |prev, curr|
      (@prime_dict[prev] .. Float::INFINITY).each do |num|
        @prime_dict.each do |key, val|
          prime = true
          if num % val == 0
            prime = false
            break
          end
        end
        if prime
          @prime_dict[curr] = num
          prime = false
          break
        end
      end
    end
  end



  def part1(input)
    check_phrases(input, :repeating_valid)
  end

  def part2(input)
    check_phrases(input, :anagram_prime)
  end

  private
  def check_phrases(input, valid_method)
    valid_count = 0
    input.split("\n").each do |phrase|
      if self.send(valid_method, phrase)
        valid_count += 1
      end
    end
    valid_count
  end

  def repeating_valid(phrase)
    words = { }
    valid = true
    phrase.split().each do |word|
      if words[word] != nil
        valid = false
      else
        words[word] = 1
      end
    end
    valid
  end

  def anagram_valid(phrase)
    return false if !repeating_valid(phrase)
    phrase = phrase.split
    phrase.each do |word|
      permutations = word.split('').permutation
      permutations.each do |perm|
        next if word == perm.join

        return false if phrase.include? perm.join('')
      end
    end
    true
  end

  def anagram_prime(phrase)
    prime_products = []
    phrase.split.each_with_index do |word, index|
      prime_products << 1
      word.split('').each do |char|
        prime_products[index] *= @prime_dict[char]
      end
    end
    prime_products.uniq.length == prime_products.length
  end


end

