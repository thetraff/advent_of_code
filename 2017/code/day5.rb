class Day5


  def part1(input)
    run_maze(input, :part1_inc)
  end

  def part2(input)
    run_maze(input, :part2_inc)
  end



  private
  def run_maze(string_input, inc_function)
    instructions = string_input.split("\n").map(&:to_i)
    curr_position = 0
    curr_val = instructions[curr_position]
    steps = 0
    while curr_val != nil
      jump_position = instructions[curr_position] + curr_position
      instructions[curr_position] = self.send(inc_function, curr_val)
      curr_position = jump_position
      steps += 1
      curr_val = instructions[curr_position]
    end
    steps
  end

  def part1_inc(input)
    input + 1
  end

  def part2_inc(input)
    input + (input >= 3 ? -1 : 1)
  end
end
