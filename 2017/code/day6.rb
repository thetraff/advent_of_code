require "prime"
class Day6

  def initialize
    @primes = []
    Prime.each(16) do |prime_number|
      @primes << prime_number
    end
  end

  def part1(string_input)
    count, config = run_algorithm(string_input)
    count
  end

  def part2(string_input)
    first_config = run_algorithm(string_input)[1]
    cycle_length = run_algorithm(first_config.join("\t"))[0]
    cycle_length
  end

  private
  def run_algorithm(string_input)
    input = string_input.split.map(&:to_i)
    seen_configs = []
    current_config = input
    count = 0

    until seen_configs.include? current_config
      seen_configs << current_config.dup
      # get the max value and the index
      max_value = current_config.max
      max_index = current_config.index(max_value)
      # set the max value to 0
      current_config[max_index] = 0
      # rotate so first val is the one after the max
      current_config.rotate!(max_index + 1)
      max_value_iter = max_value
      max_value_iter.times do |count|
        current_config[count % current_config.size] += 1
        max_value -= 1
      end
      current_config.rotate!(-1 *(max_index + 1))
      count += 1
    end
    return count, current_config
  end


  def create_prime_hash(input)
    prime_product = 1
    input.each_with_index do |val, index|
      prime_product *= (@primes[index] ** val)
    end
    prime_product
  end

end


