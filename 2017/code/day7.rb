class Day7

  def part1(string_input)
    adj_list = create_adj_list(string_input)
    root = ""
    adj_list.each do |base, val|
      if val[:children] == nil
        root = find_root(adj_list, base)
        break
      end
    end
    root
  end

  def part2(string_input)
    root = part1(string_input)
    adj_list = create_adj_list(string_input)

    wrong_node = find_off_weight(root, adj_list)
    parent = find_parent(wrong_node, adj_list)
    sums = {}
    adj_list[parent][:children].each do |child|
      sums[child] = sum_children(child, adj_list)
    end

    # get hash of values with count of each value
    freq = sums.values.inject(Hash.new(0)) { |h,v| h[v] += 1; h }
    weight_difference = freq.key(freq.values.max) - freq.key(1)
    adj_list[wrong_node][:weight] + weight_difference

  end


  private
  def find_root(adj_list, leaf)
    adj_list.each do |base, val|
      if val[:children] != nil && val[:children].include?(leaf)
        return find_root(adj_list, base)
      end
    end
    leaf
  end

  def create_adj_list(input)
    adj_list = {}
    input.split("\n").each do |line|
      base_tower = line.split("->")[0].split[0] # get the name of the base tower w/o the weight
      base_weight = line.split("->")[0].split[1].tr!('()','').to_i # get the weight of the base
      towers_held = line.split("->")[1] # get the towers it is holding up
      if towers_held
        towers_held = towers_held.split(",")
        towers_held = towers_held.map(&:split).flatten
      end

      adj_list[base_tower] = {weight: base_weight, children: towers_held}
    end
    adj_list
  end

  def sum_children(root, adj_list)
    accum = adj_list[root][:weight]
    return adj_list[root][:weight] if adj_list[root][:children] == nil
    adj_list[root][:children].each { |child| accum += sum_children(child, adj_list) }
    accum
  end

  def find_off_weight(root, adj_list)
    sums = {}
    adj_list[root][:children].each do |node|
      sums[node] = sum_children(node, adj_list)
    end

    # get hash with each different value and count of that value
    freq = sums.values.inject(Hash.new(0)) { |h,v| h[v] += 1; h }
    wrong_node = sums.key(freq.key(1))
    return root if wrong_node == nil # then this level is balanced, which means the parent is the one out of balance

    find_off_weight(wrong_node, adj_list)
  end

  def find_parent(child, adj_list)
    adj_list.each do |node, attr|
      if attr[:children] && attr[:children].include?(child)
        return node
      end
    end
  end


end
