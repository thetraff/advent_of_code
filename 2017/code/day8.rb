class Day8

  def part1(string_input)
    run_instructions(string_input)[-1]
  end

  def part2(string_input)
    run_instructions(string_input).max
  end

  def run_instructions(string_input)
    instructions = read_instructions(string_input)
    registers = {}
    max_values = []

    instructions.each do |ins|
      registers[ins[:register]] ||= 0
      register_value = registers[ins[:register]]
      condition_register = ins[:cond_expression][0]
      registers[condition_register] ||= 0
      register_cond_val = registers[condition_register]
      
      if [register_cond_val,ins[:cond_expression][2]].inject(ins[:cond_expression][1])
        #p registers[ins[:register]], ins[:value]
        registers[ins[:register]] = [registers[ins[:register]],ins[:value]].inject(ins[:operation])
      end

      max_values << registers.max_by{ |k, v| v}[1]

    end
    max_values
  end


  def read_instructions(string_input)
    instruction_strings = string_input.split("\n")
    instructions = []
    instruction_strings.each do |ins_str|
      ins = ins_str.split
      instructions << {
        register: ins[0],
        operation: convert_string_operation(ins[1]),
        value: ins[2].to_i,
        cond_expression: [ins[4], ins[5].to_sym, ins[6].to_i]
      }
    end
    instructions
  end

  def convert_string_operation(operation)
    case operation
    when "inc"
      :+
    when "dec"
      :-
    end
  end

end
