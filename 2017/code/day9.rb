class Day9

  def part1(string_input)
    filtered_input = filter_string(string_input)[0]
    level = 0
    score = 0
    filtered_input.each do |char|
      case char
      when "{"
        level += 1
        score += level
      when "}"
        level -= 1
      end
    end
    score
  end

  def part2(string_input)
    filter_string(string_input)
    #filtered_input = filter_string(string_input)
    #numbers = filtered_input.reject {|x| x.is_a? String}
    #numbers.inject(0) {|sum,x| sum + x }
  end

  private
  def count_groups(input)
    filtered_input = filter_string(input)
    character_hash = Hash.new(0)
    filtered_input.each do |char|
      character_hash[char] += 1
    end
    character_hash["{"]
  end

  def filter_string(input)
    modified_input = []
    in_garbage_string = false
    skip_flag = false
    garbage_count = 0
    input.split("").each do |character|
      if skip_flag
        skip_flag = false
        next
      end
      if in_garbage_string && character != ">" && character != "!"
        garbage_count += 1
      end
      case character
      when "{"
        modified_input << character unless in_garbage_string
      when "<"
        #modified_input << character
        in_garbage_string = true
      when ">"
        modified_input << garbage_count
        #modified_input << character
        #garbage_count = 0
        in_garbage_string = false
      when "}"
        modified_input << character unless in_garbage_string
      when "!"
        skip_flag = true
      end
    end
    modified_input
  end

end

