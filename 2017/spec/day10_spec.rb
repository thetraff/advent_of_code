require_relative 'spec_helper'

class Day10Tests < MiniTest::Test

  def test_small_knot_hash_single_round
    knot_hash = KnotHash.new([3,4,1,5], 4)
    knot_hash.run_rounds(1)
    assert_equal(12, knot_hash.list[0] * knot_hash.list[1])
  end

  def test_uniq_vals_after_running
    list = @day10.send(:run_n_times,64)
    assert_equal(list.uniq, (0..@max_int).to_a)
  end
  
  def test_xor_function
    assert_equal(64, @day10.send(:xor_sequence,[65,27,9,1,4,3,40,50,91,7,6,0,2,5,68,22]))
  end
end
