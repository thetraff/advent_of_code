require_relative 'spec_helper'

class Day12Tests < MiniTest::Test

  def setup
    @input = "0 <-> 2\n1 <-> 1\n2 <-> 0, 3, 4\n3 <-> 2, 4\n4 <-> 2, 3, 6\n5 <-> 6\n6 <-> 4, 5"
    @day12 = Day12.new
    @test_network = Network.new(@input)
  end

  def test_pipe_connection_list
    assert_equal([2], @test_network.pipe(0))
    assert_equal([1], @test_network.pipe(1))
  end
  
  def test_pipe_find_group
    #assert_equal([1], @test_network.find_group(1))
  end

  def test_pipe_find_group_length
    assert_equal(6, @test_network.find_group(0).length)
  end
end

