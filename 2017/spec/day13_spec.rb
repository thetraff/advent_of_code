require_relative 'spec_helper'

class Day13Tests < MiniTest::Test

  def setup
    @test_layer = Layer.new(5)
  end

  def test_layer_increment_function
    assert_equal(1, @test_layer.scanner[0])
    @test_layer.increment
    assert_equal(0, @test_layer.scanner[0])
    assert_equal(1, @test_layer.scanner[1])
  end

  def test_postion_at_time_step
    layer = Layer.new(5)
    assert_equal(0, layer.position_at(0))
    assert_equal(3, layer.position_at(5))
  end


  def test_layer_increment_at_end
    @test_layer.reset
    5.times { @test_layer.increment }
    assert_equal(1, @test_layer.scanner[3])
    assert_equal(3, @test_layer.current)
  end
end
