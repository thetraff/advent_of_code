require_relative 'spec_helper'

class Day15Tests < MiniTest::Test

  def setup
    @gen_a = Generator.new(65, 16807)
    @gen_b = Generator.new(8921, 48271)
    @day15 = Day15.new
  end

  def test_matching_binary_pair
    @gen_a.current = 245556042
    @gen_b.current = 1431495498
    assert @gen_a.last_16_bits == @gen_b.last_16_bits
  end
end
