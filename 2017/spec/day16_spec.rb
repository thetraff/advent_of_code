require_relative 'spec_helper'

class Day16Tests < MiniTest::Test

  def setup
    @dance_line = DanceLine.new(5)
  end

  def test_spin_move
    @dance_line.spin(3)
    assert_equal("cdeab", @dance_line.to_s)
  end

  def test_exchange_move
    @dance_line.exchange(3, 4)
    assert_equal("abced", @dance_line.to_s)
  end

  def test_partner_move
    @dance_line.partner("e", "b")
    assert_equal("aecdb", @dance_line.to_s)
  end
end
