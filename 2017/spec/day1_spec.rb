require_relative 'spec_helper'

class Day1Tests < MiniTest::Test

  def setup
    @day1 = Day1.new
  end

  def test_part_1_1
    input = "1122"
    assert_equal(3, @day1.part1(input))
  end

  def test_part_1_2
    input = "1111"
    assert_equal(4, @day1.part1(input))
  end

  def test_part_1_3
    input = "1234"
    assert_equal(0, @day1.part1(input))
  end

  def test_part_1_4
    input = "91212129"
    assert_equal(9, @day1.part1(input))
  end

  def test_part_2_1
    input = "1212"
    assert_equal(6, @day1.part2(input))
  end

  def test_part_2_2
    input = "1221"
    assert_equal(0, @day1.part2(input))
  end

  def test_part_2_3
    input = "123425"
    assert_equal(4, @day1.part2(input))
  end

  def test_part_2_4
    input = "123123"
    assert_equal(12, @day1.part2(input))
  end

  def test_part_2_5
    input = "12131415"
    assert_equal(4, @day1.part2(input))
  end

end
