require_relative "spec_helper"

class Day2Tests < MiniTest::Test

  def setup
    @day2 = Day2.new
  end

  def test_part_1
    input = "5\t 1\t 9\t 5\n 7\t 5\t 3\n 2\t 4\t 6\t 8"
    assert_equal(18,@day2.part1(input))
  end
   
  def test_min_max_row_1
    input = "5\t 1\t 9\t 5"
    assert_equal(8, @day2.send(:min_max_row, input))
  end

  def test_min_max_row_2
    input = "7\t 5\t 3"
    assert_equal(4, @day2.send(:min_max_row, input))
  end

  def test_min_max_row_3
    input = "2\t 4\t 6\t 8"
    assert_equal(6, @day2.send(:min_max_row, input))
  end

  def test_part_2
    input = "5\t 9\t 2\t 8\n 9\t 4\t 7\t 3\n 3\t 8\t 6\t 5"
    assert_equal(9,@day2.part2(input))
  end

  def test_divisible_row_1
    input = "5\t 9\t 2\t 8"
    assert_equal(4, @day2.send(:divisible_row, input))
  end

  def test_divisible_row_2
    input = "9\t 4\t 7\t 3"
    assert_equal(3, @day2.send(:divisible_row, input))
  end

  def test_divisible_row_3
    input = "3\t 8\t 6\t 5"
    assert_equal(2, @day2.send(:divisible_row, input))
  end


end

