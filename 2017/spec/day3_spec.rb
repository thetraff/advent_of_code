require_relative 'spec_helper'

class Day3Tests < MiniTest::Test

  def setup
    @day3 = Day3.new
  end

  def test_part_1_1
    input = "1"
    assert_equal(0, @day3.part1(input))
  end

  def test_part_1_2
    input = "12"
    assert_equal(3, @day3.part1(input))
  end

  def test_part_1_3
    input = "23"
    assert_equal(2, @day3.part1(input))
  end

  def test_part_1_4
    input = "25"
    assert_equal(4, @day3.part1(input))
  end

  def test_part_1_5
    input = "1024"
    assert_equal(31, @day3.part1(input))
  end

  def test_part_1_6
    input = "31"
    assert_equal(6, @day3.part1(input))
  end

  def test_part_2_1
    input = "54"
    assert_equal(57, @day3.part2(input))
  end

  def test_part_2_2
    input = "147"
    assert_equal(304, @day3.part2(input))
  end

  def test_part_2_3
    input = "362"
    assert_equal(747, @day3.part2(input))
  end

  def test_part_2_4
    input = "957"
    assert_equal(1968, @day3.part2(input))
  end

end


