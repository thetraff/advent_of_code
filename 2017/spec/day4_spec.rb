require_relative 'spec_helper'

class Day4Tests < MiniTest::Test

  def setup
    @input_1 = "aa bb cc dd ee\n aa bb cc dd aa\n aa bb cc dd aaa\n car laptop car"
    @input_2 = "abcde fghij\n abcde xyz ecdab\n a ab abc abd abf abj\n iiii oiii ooii oooi oooo\n oiii ioii iioi iiio"
    @phrases_1 = @input_1.split("\n")
    @phrases_2 = @input_2.split("\n")
    @day4 = Day4.new
  end

  def test_part_1
    assert_equal(2,@day4.part1(@input_1))
  end

  def test_input_1_1
    assert_equal(true,@day4.send(:repeating_valid, @phrases_1[0]))
  end

  def test_input_1_2
    assert_equal(false,@day4.send(:repeating_valid, @phrases_1[1]))
  end

  def test_input_1_3
    assert_equal(true,@day4.send(:repeating_valid, @phrases_1[2]))
  end

  def test_input_1_4
    assert_equal(false,@day4.send(:repeating_valid, @phrases_1[3]))
  end

  def test_part_2
    assert_equal(3, @day4.part2(@input_2))
  end

  def test_input_2_1
    assert_equal(true, @day4.send(:anagram_valid, @phrases_2[0]))
  end

  def test_input_2_2
    assert_equal(false, @day4.send(:anagram_valid, @phrases_2[1]))
  end

  def test_input_2_3
    assert_equal(true, @day4.send(:anagram_valid, @phrases_2[2]))
  end

  def test_input_2_4
    assert_equal(true, @day4.send(:anagram_valid, @phrases_2[3]))
  end

  def test_input_2_5
    assert_equal(false, @day4.send(:anagram_valid, @phrases_2[4]))
  end



end

