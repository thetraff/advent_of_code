require_relative 'spec_helper'

class Day5Tests < MiniTest::Test
  
  def setup
    @input = "0\n3\n0\n1\n-3\n"
    @day5 = Day5.new
  end

  def test_part_1
    assert_equal(5, @day5.part1(@input))
  end

  def test_part_1_inc
    assert_equal(1, @day5.send(:part1_inc, 0))
  end

  def test_part_2
    assert_equal(10, @day5.part2(@input))
  end

  def test_part_2_inc_greater_than_3
    assert_equal(10, @day5.send(:part2_inc, 11))
  end

  def test_part_2_inc_less_than_3
    assert_equal(3, @day5.send(:part2_inc, 2))
  end

end

