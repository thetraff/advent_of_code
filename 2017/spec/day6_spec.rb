require_relative 'spec_helper'

class Day6Tests < MiniTest::Test

  def setup
    @input = "0\t2\t7\t0"
    @day6 = Day6.new
  end

  def test_part_1
    assert_equal(5, @day6.part1(@input))
  end

  def test_part_2
    assert_equal(4, @day6.part2(@input))
  end

end
