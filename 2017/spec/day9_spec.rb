require_relative 'spec_helper'

class Day9Tests < MiniTest::Test

  def setup 
    @input = {
      "{}"=> {score: 1, groups: 1},
      "{{{}}}"=> {score: 6, groups: 3},
      "{{},{}}"=> {score: 5, groups: 3},
      "{{{},{},{{}}}}"=> {score: 16, groups: 6},
      "{<{},{},{{}}>}"=> {score: 1, groups: 1},
      "{<a>,<a>,<a>,<a>}"=> {score: 1, groups: 1},
      "{{<a>},{<a>},{<a>},{<a>}}"=> {score: 9, groups: 5},
      "{{<!>},{<!>},{<!>},{<a>}}"=> {score: 3, groups: 2}
    }

    @input_2 = "<{o\"i!a,<{i<a>"

    @day9 = Day9.new
  end

  def test_part_2
    assert_equal([10], @day9.part2(@input_2))
  end

  def test_count_groups
    @input.each do |input, results|
      assert_equal(results[:groups], @day9.send(:count_groups, input))
    end
  end
  
  def test_filter_string
    assert_equal(["{",10,"}"],@day9.send(:filter_string, @input.keys[4]))
  end

end
