
class Day1

  def num_sequence(input)
    input.scan(/[+-][0-9]+\n/).map(&:to_i)
  end


  def part1(input)
    num_sequence = num_sequence(input)
    frequency = 0
    num_sequence.each { |num| frequency += num }
    frequency
  end
  
  def part2(input)
    num_sequence = num_sequence(input)
    frequency = 0
    seen_frequencies = {0 => 1}
    num_sequence.cycle do |num|
      frequency += num
      break if seen_frequencies[frequency]
      seen_frequencies[frequency] = 1
    end
    frequency
  end

end
