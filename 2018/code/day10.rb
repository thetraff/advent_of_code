class Day10

  class Flare
    attr_accessor :curr_position

    def initialize(curr_x_position, curr_y_position, x_velocity, y_velocity)
      @curr_position = [curr_x_position.to_i, curr_y_position.to_i]
      @velocity = [x_velocity.to_i, y_velocity.to_i]
    end

    def move
      @curr_position[0] += @velocity[0]
      @curr_position[1] += @velocity[1]
    end

    def move_back
      @curr_position[0] -= @velocity[0]
      @curr_position[1] -= @velocity[1]
    end


    def move_n(steps)
      @curr_position[0] += (@velocity[0] * steps)
      @curr_position[1] += (@velocity[1] * steps)
    end

    def x_pos
      @curr_position[0]
    end
    
    def y_pos
      @curr_position[1]
    end

  end

  def most_left(positions)

    left_most = [999999, -999999]
    positions.each do |pos|
      if pos[0] < left_most[0] && pos[1] > left_most[1]
        left_most = pos
      end
    end
    left_most
  end

  def longest_segment(start_position, positions)
    longest_segment = 0
    positions.each do |pos|
      distance_from_start = Math.sqrt((pos[0] - start_position[0])**2 + (pos[1] - start_position[1])**2)
      if distance_from_start > longest_segment
        longest_segment = distance_from_start
      end
    end
    longest_segment
  end

  def longest_segment_enum(flares)
    enum = Enumerator.new do |y|
      longest = Float::INFINITY
      loop do
        y << longest
        flares.map(&:move)
        positions = flares.map(&:curr_position)
        longest = longest_segment(most_left(positions), positions)
      end
    end
    enum
  end

  def display_flares(flares)
    positions = flares.map(&:curr_position)
    x_min, x_max = flares.map(&:x_pos).minmax
    y_min, y_max = flares.map(&:y_pos).minmax

    grid = {}
    (x_min..x_max).to_a.each do |i|
      (y_min..y_max).to_a.each do |j|
        if positions.index([i,j])
          grid[[i,j]] = '#'
        else
          grid[[i,j]] = '.'
        end
        print(grid[[i,j]])
      end
      puts()
    end
  end



  def part1(input)
    flare_inputs = input.gsub(/\s+/,"").scan(/position=<(-?\d+),(-?\d+)>velocity=<(-?\d+),(-?\d+)>/)
    flares = []
    flare_inputs.each do |flare_input|
      flares << Flare.new(*flare_input)
    end

    lengths = longest_segment_enum(flares)
    prev_length = lengths.first
    lengths.each do |l|
      if l > prev_length
        break
      else
        prev_length = l
      end
    end

    flares.map(&:move_back)
    display_flares(flares)



    
  end

  def part2(input)
    flare_inputs = input.gsub(/\s+/,"").scan(/position=<(-?\d+),(-?\d+)>velocity=<(-?\d+),(-?\d+)>/)
    flares = []
    flare_inputs.each do |flare_input|
      flares << Flare.new(*flare_input)
    end

    second_count = 0
    lengths = longest_segment_enum(flares)
    prev_length = lengths.first
    lengths.each do |l|
      if l > prev_length
        break
      else
        prev_length = l
      end
      second_count += 1
    end
    second_count - 1

  end

end
