require 'set'
class Day2

  def part1(input)
    box_ids = input.scan(/[a-z]+\n/)
    letter_twice, letter_thrice = 0,0
    box_ids.each do |id|
      chars_in_id = {}
      id.split("").each do |char|
        if chars_in_id.include?(char)
          chars_in_id[char] += 1
        else
          chars_in_id[char] = 1
        end
      end
      letter_twice += 1 if chars_in_id.has_value?(2)
      letter_thrice += 1 if chars_in_id.has_value?(3)
    end
    letter_twice * letter_thrice
  end



  def get_columns(array, num_columns)

    num_rows = array.count / num_columns

    columns = []
    num_columns.times do |column|
      p column
      p (0..num_rows).to_a.product([column]).collect { |index| array[index]}
    end
    columns
  end
      


  def part2(input)
    box_ids = input.scan(/[a-z]+\n/)
    box_ids = box_ids.map(&:strip).map { |id| id.split('') }

    matching = []
    ans = []


    box_ids.each do |id|
      box_ids.each do |comparison_id|
        diff_count = 0
        id.each_index do |index|
          if id[index] != comparison_id[index]
            diff_count += 1
          end
          break if diff_count > 1
        end
        if diff_count == 1
          matching << id
          matching << comparison_id
          comparison_id.each_with_index do |char, index|
            ans << char if id[index] == char
          end
          break
        end
      end
    end

    matching = matching.uniq!
		p matching
    (matching[0] & matching[1]).join

  end
end
