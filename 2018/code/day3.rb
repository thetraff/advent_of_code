class Day3

  class Claim

    attr_accessor :id, :left_distance, :top_distance, 
      :width, :height
    
    def initialize(claim_string)
      @id, @left_distance, @top_distance, @width, @height = claim_string.match(/#(\d+) @ (\d+),(\d+): (\d+)x(\d+)/).captures
      @id = @id.to_i
      @left_distance = @left_distance.to_i
      @top_distance = @top_distance.to_i
      @width = @width.to_i
      @height = @height.to_i

    end

    def x_coords
      (@left_distance..(@left_distance + @width - 1)).to_a
    end

    def y_coords
      (@top_distance..(@top_distance + @height - 1)).to_a
    end
  end

  def part1(input)
    claims = input.scan(/#\d+ @ \d+,\d+: \d+x\d+/)
    overlapping_coords = {}
    claims = claims.map { |c| Claim.new(c) }

    #❯ time ruby advent.rb 2018 3 1
    #Day 3, part 1
    #100261
    #ruby advent.rb 2018 3 1  1.20s user 0.04s system 99% cpu 1.241 total
    claims.each_with_index do |claim, index|
      coords = claim.x_coords.product(claim.y_coords)
      coords.each do |coord|
        if overlapping_coords[coord]
          overlapping_coords[coord] += 1
        else
          overlapping_coords[coord] = 1
        end
      end
    end


    #❯ time ruby advent.rb 2018 3 1
    #Day 3, part 1
    #100261
    #ruby advent.rb 2018 3 1  12.35s user 0.05s system 99% cpu 12.425 total

    #initial solution was real slow
    #claims.each_with_index do |claim_1, index|
    #  claims[(index+1)..-1].each do |claim_2|
		#    #puts "comparing #{claim_1.id} and #{claim_2.id}"
    #    x_overlap = claim_1.x_coords & claim_2.x_coords
    #    y_overlap = claim_1.y_coords & claim_2.y_coords
    #    x_overlap.product(y_overlap).each do |coord|
    #      if overlapping_coords[coord]
    #        overlapping_coords[coord] += 1
    #      else
    #        overlapping_coords[coord] = 1
    #      end
    #    end
    #  end
    #end

    return overlapping_coords.reject { |k,v| v < 2 }.length

  end


  def part2(input)
    claims = input.scan(/#\d+ @ \d+,\d+: \d+x\d+/)
    overlapping_coords = {}
    claims = claims.map { |c| Claim.new(c) }

    claim_ids = claims.map(&:id)



    claims.each_with_index do |claim, index|
      coords = claim.x_coords.product(claim.y_coords)
      coords.each do |coord|
        if overlapping_coords[coord]
          overlapping_coords[coord] << claim.id
        else
          overlapping_coords[coord] = [claim.id]
        end
      end
    end

    overlapping_coords.reject { |k,v| v.length < 2 }.each do |coord, ids|
      claim_ids = claim_ids - ids
    end

    claim_ids[0]



  end
end
