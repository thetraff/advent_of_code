require 'date'
class Day4

  def parse_input(str_log)
    captures = str_log.match(/\[(\d+-\d+-\d+ \d+:\d+)\] \w+ (\w+|#\d+)/).captures
    [DateTime.parse(captures[0]), captures[1]]
  end



  def part1(input)
    logs = input.scan(/^\[\d+-\d+-\d+ \d+:\d+\] \w+ \S+/)
    logs = logs.map { |l| parse_input(l) }
    logs.sort_by { |l| l[0] }

    p logs.map { |l| [l[0].to_s, l[1]] }

  end

  def part2(input)
    
  end

end

