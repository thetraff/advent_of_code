#/usr/bin/env ruby

def run_day(year, day, part)
  Dir["./#{year}/code/*.rb"].each {|file| require file }

  input_file_path = "#{year}/input/day#{day}_input.txt"
  input = File.read(input_file_path)
  day_class = Object.const_get("Day#{day}").new
  
  if part == 1 || part == nil
    puts "Day #{day}, part 1"
    if day_class.respond_to?(:part1)
        p day_class.part1(input)
    else
        puts "not implemented"
    end
  end
  if part == 2 || part == nil
    puts "Day #{day}, part 2"
    if day_class.respond_to?(:part2)
        p day_class.part2(input)
    else
        puts "not implemented"
    end
  end
end

run_day(ARGV[0].to_i, ARGV[1].to_i, ARGV[2].to_i)
